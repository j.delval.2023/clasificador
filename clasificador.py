import sys

def es_correo_electronico(string):
    if '@' in string and '.' in string.split('@')[1]:
        return True
    return False


def es_entero(string):
    try:
        int(string)
        return True
    except ValueError:
        return False


def es_real(string):
    try:
        float(string)
        return True
    except ValueError:
        return False


def evaluar_entrada(string):
    if not string:
        return None

    if es_correo_electronico(string):
        return "Es un correo electrónico."

    if es_entero(string):
        return "Es un entero."

    if es_real(string):
        return "Es un número real."

    return "No es ni un correo, ni un entero, ni un número real."

def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()
